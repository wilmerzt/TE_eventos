<%@page import="com.emergentes.modelo.Seminario"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Seminario lib = (Seminario) request.getAttribute("lib");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            input[type=text], select {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }
            input[type=date], select {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }
            input[type=number], select {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #4CAF50;
                color: white;
                padding: 14px 20px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #45a049;
            }
            .retornar {
                width: 100%;
                background-color: red;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
            div {
                border-radius: 5px;
                background-color: #f2f2f2;
                padding: 20px;
            }
            .tirulo{
                font-family: cursive;
            }
        </style>
    </head>
    <body align="center">
        <h1 class="tirulo">
            <c:if test="${lib.id == 0}">
                Nuevo Evento
            </c:if>
            <c:if test="${lib.id != 0}">
                Editar Evento
            </c:if>
        </h1>
        <a  class="retornar" href="MainController">Regresar</a>
        <div align="center">
            <form action="MainController" method="post">

                <table>
                    <input type="hidden" name="id" value="${lib.id}">
                    <tr>
                        <td>Título</td>
                        <td><input type="text" name="titulo" value="${lib.titulo}" required></td>
                    </tr>
                    <tr>
                        <td>Expositor</td>
                        <td><input type="text" name="expositor" value="${lib.expositor}" required> </td>
                    </tr>
                    <tr>
                        <td>Fecha</td>
                        <td><input type="date" name="fecha" value="${lib.fecha}" required></td>
                    </tr>                
                    <tr>
                        <td>Hora</td>
                        <td><input type="text" name="hora" value="${lib.hora}" required></td>
                    </tr>
                    <tr>
                        <td>Cupo</td>
                        <td><input type="number" name="cupo" value="${lib.cupo}" required></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="submit" value="Enviar" ></td>
                    </tr>

                </table>
            </form>
        </div>
    </body>
</html>
