<%@page import="java.util.List"%>
<%@page import="com.emergentes.modelo.Seminario"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    List<Seminario> lista = (List<Seminario>) request.getAttribute("lista");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            table {
                border-collapse: collapse;
                width: 100%;
            }

            th, td {
                text-align: left;
                padding: 8px;
            }
            .nuevo {
                width: 100%;
                background-color: dodgerblue;
                color: white;
                padding: 10px 25px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
            .editar {
                width: 100%;
                background-color: orange;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
            .eliminar {
                width: 100%;
                background-color: red;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
            .tirulo{
                   font-family: cursive;
            }
            tr:nth-child(even) {background-color: #f2f2f2;}
        </style>
    </head>
    <body align="center">
        <h1 class="tirulo">Listado de Seminarios</h1>
        <p><a href="MainController?op=nuevo" class="nuevo">Nuevo</a></p>
        <table>
            <tr>
                <th>Id</th>
                <th>Titulo</th>
                <th>Expositor</th>
                <th>Fecha</th>
                <th>Hora</th>
                <th>Cupo</th>
                <th></th>
                <th></th>
            </tr>
            <c:forEach var="item" items="${lista}">
                <tr>
                    <td>${item.id}</td>
                    <td>${item.titulo}</td>
                    <td>${item.expositor}</td>
                    <td>${item.fecha}</td>
                    <td>${item.hora}</td>
                    <td>${item.cupo}</td>
                    <td><a class="editar" href="MainController?op=editar&id=${item.id}" >Editar</a></td>
                    <td><a class="eliminar" href="MainController?op=eliminar&id=${item.id}" 
                           onclick="return(confirm('Está seguro ???'))">Eliminar</a></td>
                </tr>
            </c:forEach>
        </table>

    </body>
</html>
