package com.emergentes.controlador;

import com.emergentes.modelo.Seminario;
import com.emergentes.utiles.ConexionDB;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "MainController", urlPatterns = {"/MainController"})
public class MainController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String op;
            op = (request.getParameter("op") != null) ? request.getParameter("op") : "list";
            ArrayList<Seminario> lista = new ArrayList<Seminario>();
            ConexionDB canal = new ConexionDB();
            Connection conn = canal.conectar();
            PreparedStatement ps;
            ResultSet rs;
            int id;
            if (op.equals("list")) {
                // Para listar los datos
                String sql = "SELECT * FROM seminarios";
                // Consulta de selección y almacenarlo en una colección
                ps = conn.prepareStatement(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    Seminario lib = new Seminario();
                    lib.setId(rs.getInt("id"));
                    lib.setTitulo(rs.getString("titulo"));
                    lib.setExpositor(rs.getString("expositor"));
                    lib.setFecha(rs.getString("fecha"));
                    lib.setHora(rs.getString("hora"));
                    lib.setCupo(rs.getInt("cupo"));
                    lista.add(lib);
                }
                request.setAttribute("lista", lista);
                // Enviar al index.jsp para mostrar la informacion
                request.getRequestDispatcher("index.jsp").forward(request, response);
            }
            if (op.equals("nuevo")) {
                // Instanciar un objeto de la clase Libro
                Seminario li = new Seminario();

                System.out.println(li.toString());

                // El objeto se pone como atributo de request
                request.setAttribute("lib", li);
                // Redireccionar a editar.jsp
                request.getRequestDispatcher("editar.jsp").forward(request, response);
            }
            if (op.equals("editar")) {
                // Instanciar un objeto de la clase Libro
                id = Integer.parseInt(request.getParameter("id"));
                try {
                    Seminario lib1 = new Seminario();

                    ps = conn.prepareStatement("select * from seminarios where id = ?");
                    ps.setInt(1, id);
                    rs = ps.executeQuery();

                    if (rs.next()) {
                        lib1.setId(rs.getInt("id"));
                        lib1.setTitulo(rs.getString("titulo"));
                        lib1.setExpositor(rs.getString("expositor"));
                        lib1.setFecha(rs.getString("fecha"));
                        lib1.setHora(rs.getString("hora"));
                        lib1.setCupo(rs.getInt("cupo"));

                    }

                    request.setAttribute("lib", lib1);
                    request.getRequestDispatcher("editar.jsp").forward(request, response);
                } catch (SQLException ex) {
                    Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
            if (op.equals("eliminar")) {
                // Obtener el id
                id = Integer.parseInt(request.getParameter("id"));
                // Realizar la eliminación en la base de datos
                String sql = "delete from seminarios where id = ?";
                ps = conn.prepareStatement(sql);
                ps.setInt(1, id);
                ps.executeUpdate();
                // Redireccionar a MainController
                response.sendRedirect("MainController");
            }
        } catch (SQLException ex) {
            System.out.println("ERROR AL CONECTAR " + ex.getMessage());
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            int id = Integer.parseInt(request.getParameter("id"));
            System.out.println("Valor de ID " + id);
            String titulo = request.getParameter("titulo");
            String expositor = request.getParameter("expositor");
            String fecha = request.getParameter("fecha");
            String hora = request.getParameter("hora");
            int cupo = Integer.parseInt(request.getParameter("cupo"));

            Seminario lib = new Seminario();
            lib.setId(id);
            lib.setTitulo(titulo);
            lib.setExpositor(expositor);
            lib.setFecha(fecha);
            lib.setHora(hora);
            lib.setCupo(cupo);

            ConexionDB canal = new ConexionDB();
            Connection conn = canal.conectar();
            PreparedStatement ps;
            ResultSet rs;

            if (id == 0) {
                // Nuevo registro
                String sql = "insert into seminarios (titulo, expositor, fecha, hora, cupo) values (?,?,?,?,?)";
                ps = conn.prepareStatement(sql);
                ps.setString(1, lib.getTitulo());
                ps.setString(2, lib.getExpositor());
                ps.setString(3, lib.getFecha());
                ps.setString(4, lib.getHora());
                ps.setInt(5, lib.getCupo());
                ps.executeUpdate();
            } else {
                String sql1 = "update seminarios set titulo=?, expositor=?, fecha=?, hora=?, cupo=? where id=?";
                
                try {
                    ps = conn.prepareStatement(sql1);
                    ps.setString(1, lib.getTitulo());
                    ps.setString(2, lib.getExpositor());
                    ps.setString(3, lib.getFecha());
                    ps.setString(4, lib.getHora());
                    ps.setInt(5, lib.getCupo());
                    ps.setInt(6, lib.getId());
                    ps.executeUpdate();

                } catch (SQLException ex) {
                    Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            response.sendRedirect("MainController");
        } catch (SQLException ex) {
            System.out.println("Error en SQL " + ex.getMessage());
        }
    }

}
